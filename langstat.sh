#!/bin/bash

echo && echo
echo "**********************************************************"
echo "*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*"
echo "*!                                                      !*"
echo "*!     Language Statistics script by Natan FOURIÉ.      !*"
echo "*!                                                      !*"
echo "*!------------------------------------------------------!*"
echo "*!                                                      !*"
echo "*!         Retrouvez tous mes projects Github:          !*"
echo "*!           https://github.com/Natizyskunk             !*"
echo "*!                                                      !*"
echo "*!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*"
echo "**********************************************************"
echo && echo

echo "Etes-vous sur de vouloir lancer l'analyse statistique du dictionnaire ? (non pour annuler) [oui/non]"
read DOSETUP

# On lance l'anayse statistique du dictionnaire si l'utilisateur répond 'oui', 'y', 'yes' ou 'o' à la question.
if [[ $DOSETUP = y || $DOSETUP = yes || $DOSETUP = o || $DOSETUP = oui ]] ;
then
	
	
	# Vérification de la présence des paamètres.
	if [ $# -eq 0 ] ;
	then
		echo
		# Vérification de la présence du premier paramètre obligatoire indiquant le nom du fichier dictionnaire à utiliser.
		echo "Pas de paramètre, Merci d'en renseigner au moins 1."
	else
		# Vérfication de la présence du fichier dictionnaire.
		if [[ $1 != dico.txt ]] ;
		then
			echo
			echo "Le fichier dico.txt est introuvable. Vérifiez l'orthographe du paramètre et qu'il soit bien dans le même répertoire que le script."
		else
			# Vérification de la différences des paramètres si un deuxième est renseigné.
			if [[ $1 = $2 ]] ;
			then
				echo
				echo "Les 2 parametres sont identiques, Merci de modifier le second."
			fi
		fi
	fi
	
	# Déclarations de mes variables.
	dico=$1
	noDoublonsDico="dico_sans_doublons.txt"
	newDico="newDico.txt"
	uniqWord="uniq $dico"
	
	echo
	# Supprimer les doublons.
	echo "1. Les doublons sont supprimés et un nouveau fichier" \"$noDoublonsDico\" "est créé."
	$uniqWord $noDoublonsDico
	
	# Trier par ordre alphabétique.
	echo "2. Tri du fichier" \"$noDoublonsDico\" "vers le fichier final" \"$newDico\""."
	sort -o $newDico $noDoublonsDico
	
	# Supprimer le nom du fichier derrière le nombre pour pouvoir l'affichier joliment plus tard.
	wordNumber=`wc -w $newDico | awk -F ' ' '{print $1}'`
	carNumber=`wc -m $newDico | awk -F ' ' '{print $1}'`
	
	echo "3. Il y a" $wordNumber "mots pour un total de" $carNumber "lettres". 
	echo
	
	# Trier par ordre alphabétique.
	if [[ $2 = --tri-alphabetique ]] ;
	then
		for alphabet in {A..Z}
		do
			echo $alphabet "-" $(grep -ic $alphabet "$newDico")
		done | sort
	# Sinon Trier du plus grand au plus petit.
	else
		echo "- resultats:"
		for alphabet in {A..Z}
		do
			echo $(grep -ic $alphabet "$newDico") "-" $alphabet
		done | sort -rn
	fi
	
	# Suppression des fichiers temporaires de travail.
	rm -f dico_sans_doublons.txt
	rm -f newDico.txt	

# Sinon si l'utilisateur répond 'non', 'no', 'nope' ou 'n' on annule l'anayse statistique du dictionnaire.
elif [[ $DOSETUP = n || $DOSETUP = no || $DOSETUP = non || $DOSETUP = nope ]] ;
then
	echo "L'analyse a été annulée!"
# Sinon on alerte l'utilisateur que le programme n'a pas compris sa réponse, on le lui redonne les réponses possibles et on arrête le programme.
else
	echo "!!Alert!!->Commande incomprise. Commandes Dispo: y,yes,o,oui/n,no,non,nope"
	echo "L'analyse va s'arreter!"
fi