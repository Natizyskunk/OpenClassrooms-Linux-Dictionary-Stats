<h1>OpenClassrooms-Linux-Dictionary-Stats</h1>

<p>C'est le moment de mettre en pratique vos connaissances en Bash. Cet exercice va être l'occasion de produire votre propre script Bash, de faire appel à votre créativité, vos capacités de recherches sur le Net et de lecture du manuel. Ouf, tout ça ! :o)</p>

<img src="/preview.png" alt="Preview">

<p>- <strong><a href="https://openclassrooms.com/courses/reprenez-le-controle-a-l-aide-de-linux/exercises/85">Se rendre sur l'activité OpenClassrooms.</a><br /></strong><BR />
- <strong><a href="https://openclassrooms.com/courses/reprenez-le-controle-a-l-aide-de-linux">Se rendre sur le cours Linux d'OpenClassrooms.</a><br /></strong></p>

<h2>Le script Bash que vous devez créer</h2>

<p>Je vais vous demander de cr&eacute;er un script Bash qui&nbsp;fournit des <strong>statistiques sur l'utilisation des lettres dans une langue</strong>. Pour cela, vous allez devoir vous baser sur un fichier de dictionnaire contenant tous les mots de la
    langue fran&ccedil;aise que vous pouvez t&eacute;l&eacute;charger ci-dessous :</p>
<p><strong><a href="http://www.siteduzero.com/uploads/fr/ftp/mateo21/cpp/dico.zip">T&eacute;l&eacute;charger le fichier dictionnaire</a><br /></strong></p>
<p>Le fichier, tout en majuscules, contient un mot par ligne, comme ceci :</p>
<pre>
	ABAISSA
	ABAISSABLE
	ABAISSABLES
	ABAISSAI
	ABAISSAIENT<br />
	[...]
</pre>

<h3>Mission n&deg;1&nbsp;: combien de fois est utilis&eacute;e chaque lettre ?</h3>

<p>A partir de ce fichier (dico.txt) vous devez extraire le nombre de mots utilisant chaque lettre de l'alphabet de A &agrave; Z. Le script que vous allez cr&eacute;er (langstat.sh) prendra en param&egrave;tre le nom du fichier dictionnaire &agrave; analyser
    :</p>
<pre>./langstat.sh dico.txt</pre>
<p>Le r&eacute;sultat affich&eacute; dans la console devrait ressembler &agrave; ceci, de la lettre la plus utilis&eacute;e &agrave; la moins utilis&eacute;e:</p>
<pre>
    278814 - E
    229938 - A
    219131 - I
    210391 - R
    207889 - S
    179165 - N
    176030 - T
    158282 - O
    [...]
</pre>
<p>Cela signifie que la lettre E appara&icirc;t dans 278814 mots du dictionnaire, la lettre A dans 229938 mots, etc.</p>

<h3>Mission n&deg;2&nbsp;: un peu de cr&eacute;ativit&eacute; !</h3>

<p>Une fois que vous aurez r&eacute;alis&eacute; la premi&egrave;re mission, vous devrez cr&eacute;er une fonctionnalit&eacute; suppl&eacute;mentaire dans votre script. Cette fonctionnalit&eacute; pourra&nbsp;&ecirc;tre activ&eacute;e par la pr&eacute;sence
    d'un second param&egrave;tre que vous devez inventer :</p>
<pre>./langstat.sh dico.txt --second-parametre</pre>
<p><strong>Soyez cr&eacute;atifs</strong> ! Le second param&egrave;tre peut simplement changer la fa&ccedil;on dont sont affich&eacute;es les donn&eacute;es, ou peut fournir d'autres statistiques. Plus vous serez original et cr&eacute;atif, meilleure sera
    votre note. ;o)&nbsp;</p>

<h2>Le&nbsp;bar&egrave;me</h2>

<p>Vous serez not&eacute; sur plusieurs crit&egrave;res :</p>

<ul>
    <li>Afficher le nombre de fois que chaque lettre est utilis&eacute;e au moins une fois dans un mot (mission n&deg;1)</li>
    <li>V&eacute;rifier la pr&eacute;sence du param&egrave;tre indiquant le nom du fichier dictionnaire &agrave; utiliser</li>
    <li>V&eacute;rifier que le fichier dictionnaire existe bel et bien</li>
    <li>Ne pas laisser de fichier temporaire de travail sur le disque</li>
    <li>Proposer une seconde fonctionnalit&eacute; originale &agrave; partir d'un second param&egrave;tre (mission n&deg;2)</li>
    <li>Fournir quelques commentaires dans le script expliquant son fonctionnement</li>
</ul>

<p>Vous connaissez la plupart des commandes vous permettant de r&eacute;aliser ce script : vous les avez apprises dans ce cours. Cependant, la lecture du manuel&nbsp;et&nbsp;quelques recherches sur le Net vous seront probablement indispensables.</p>
<p>A vous de jouer !</p>
<p><em><b>NB:</b> il n'est pas n&eacute;cessaire de fournir le fichier dictionnaire dans votre ZIP. Les personnes qui vous noteront l'auront d&eacute;j&agrave;.</em></p>
